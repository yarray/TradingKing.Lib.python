from distutils.core import setup

setup(
    name='tklib',
    version='0.1dev',
    packages=['tklib'],
    license='',
    long_description=open('README.md').read(),
    install_requires=[
        'kafka-python',
    ], )
