'''
Logger using stdout or kafka

log = logger()  # logger to stdout if no LOG_SERVICE_HOST exists, or to kafka
log.log('gateway.media.twitter.hot-tweets', {
    'id': '-999',
    'author': 'nobody',
    'retweet_count': 99,
    'text': '...'
})
'''
import os
import json
from kafka import KafkaProducer


def abs_topic(topic):
    return 'tradingking.' + topic


class KafkaLogger(object):
    def __init__(self, host, port):
        server = '{}:{}'.format(host, port)
        self.producer = KafkaProducer(
            bootstrap_servers=server,
            value_serializer=lambda v: json.dumps(v).encode('utf-8'))

    def log(self, topic, data):
        self.producer.send(abs_topic(topic), data)
        self.producer.flush()


class StdLogger(object):
    def log(self, topic, data):
        print(
            json.dumps({
                'topic': abs_topic(topic),
                'data': data
            }), flush=True)


def logger(host=None, port=None):
    if host is None:
        host = os.environ.get('LOG_SERVICE_HOST')
    if host is None:
        return StdLogger()
    else:
        return KafkaLogger(host, port or os.environ.get('LOG_SERVICE_PORT')
                           or 9092)
