'''
functional reactive programming facilities to handle streams

naming convention:
f: function
ts: time serie (generator)
'''
import time
import queue
from collections import defaultdict, deque
import threading
from functools import wraps


def curry(func, *args, **kwargs):
    '''
    https://gist.github.com/Djexus/1193399/d9214b3cfdcfb112e7c1ea0f0e8e74f005c7bbc8

    This decorator make your functions and methods curried.
    Limitation: cannot handle default args
    Usage:
    >>> adder = curry(lambda x, y: x + y)
    >>> adder(2, 3)
    5
    >>> adder(2)(3)
    5
    >>> adder(y = 3)(2)
    5
    '''

    if (len(args) + len(kwargs)) >= func.__code__.co_argcount:
        return func(*args, **kwargs)

    @wraps(func)
    def wrapper(*x, **y):
        return curry(func, *(args + x), **dict(kwargs, **y))

    return wrapper


def pipe(*funcs):
    '''
    >>> add1 = lambda x: x + 1
    >>> add2 = lambda x: x + 2
    >>> pipe(add1, add2)(2)
    5
    '''

    def func(ts):
        res = funcs[0](ts)
        for f in funcs[1:]:
            res = f(res)
        return res

    return func


def repeat(f, interval):
    ''' repeat operation after time intervals
    >>> g = repeat(lambda: 1, 0)
    >>> next(g)
    1
    >>> next(g)
    1
    '''
    while True:
        yield f()
        time.sleep(interval)


def tunnel(buffer_size=1):
    '''
    create a normal infinite generator (gen) which can be
    continuously pushed to (send), useful when producers
    are asyncronous with callback
    >>> gen, send = tunnel(10)
    >>> send(1)
    >>> send(5)
    >>> g = gen()
    >>> next(g)
    1
    >>> next(g)
    5
    '''
    buffer = queue.Queue(buffer_size)

    def gen():
        while True:
            yield buffer.get()

    def send(msg):
        buffer.put(msg)

    return gen, send


@curry
def changed(eq, ts):
    ''' keep only changed event given equal function eq
    >>> l = [1, 2, 2, 3, 5, 6, 8, 9]
    >>> list(changed(lambda x, y: y - x <= 1, l))
    [1, 5, 8]
    '''
    last = None
    for e in ts:
        if last and eq(last, e):
            last = e
            continue
        yield e
        last = e


@curry
def each(f, ts):
    ''' for each event do an action and pass through the original value
    >>> l = [1, 2]
    >>> list(each(lambda x: print(x + 1), l))
    2
    3
    [1, 2]
    '''
    for e in ts:
        f(e)
        yield e


def fmap(f):
    return lambda xs: map(f, xs)


def flatten(ts):
    for xs in ts:
        for x in xs:
            yield x


@curry
def flush(f, ts):
    ''' non-generator function, consume the generator with function f
    >>> def gen():
    ...     for i in range(2):
    ...         yield i
    >>> flush(print, gen())
    0
    1
    '''
    for x in ts:
        f(x)


@curry
def groupby(key_func, reducer, key_kept, ts):
    '''
    divide points into several sub streams according to key_func,
    apply reducer, and merge the result. keys have to be regularly forgotten
    or the memory will eventually exloded, so users have to decide how many
    keys they want to remember (key_kept)

    Parameters
    ----------
    key_func : a -> str
        function to get grouping key for an item
    reducer : [a], a -> [a]
        generate a child sequence with a new item and the history
    key_kept : int
        keep how many keys at most to avoid eating up memory
    ts : [a]
        source item stream

    Doctest
    -------
    >>> s = [(0, 100), (1, 5), (0, 101), (0, 101), (1, 10), (0, 200)]
    >>> def reducer(history, point):
    ...     if len(history) == 0:
    ...         yield point
    ...     else:
    ...         last = history[-1][1]
    ...         if (point[1] - last) / last > 0.5:
    ...             yield point
    >>> list(groupby(lambda x: x[0], reducer, 10, s))
    [(0, 100), (1, 5), (1, 10), (0, 200)]
    '''
    buffer = defaultdict(list)
    keys = []

    for x in ts:
        key = key_func(x)
        for y in reducer(buffer[key], x):
            yield y
        buffer[key].append(x)
        if key not in keys:
            keys.append(key)
            # remove oldest key if overflow
            if len(keys) > key_kept:
                del buffer[keys[0]]
                keys.pop(0)
        else:
            # update key sorting, newest updated at last
            keys.remove(key)
            keys.append(key)


def tee(ts, n=2):
    '''
    thread safe version of itertools.tee,
    code from https://docs.python.org/2/library/collections.html
    >>> s = [1, 2, 3]
    >>> a, b = tee(s)
    >>> b = map(lambda x: x + 10, b)
    >>> next(a)
    1
    >>> next(b)
    11
    >>> list(a)
    [2, 3]
    >>> list(b)
    [12, 13]
    '''
    it = iter(ts)
    deques = [deque() for i in range(n)]

    lock = threading.Lock()

    def gen(local_deque):
        while True:
            if not local_deque:  # when the local deque is empty
                with lock:
                    new_val = next(it)  # fetch a new value and
                for q in deques:  # load it to all the deques
                    q.append(new_val)
            yield local_deque.popleft()

    return tuple(gen(d) for d in deques)


if __name__ == '__main__':
    import doctest
    print(groupby.__doc__)
    doctest.testmod()
